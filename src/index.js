import 'phaser'

/*
*  TODO: animations for cards
*  TODO: background sound
*  TODO: click sound, flip sound
* */
let controlEnable = false;

function tableCreate() {
    let resultTable = JSON.parse(localStorage.getItem("resultTable")) || []
    if (!resultTable.length) {
        return
    }
    let oldTable = document.getElementById("tableRecords")
    if(oldTable) {
        oldTable.remove()
    }
    let body = document.getElementById('records');
    let tbl = document.createElement('table');
    tbl.setAttribute("id", "tableRecords");
    let tbdy = document.createElement('tbody');
    for (let i = 0; i < resultTable.length; i++) {
        let tr = document.createElement('tr');
        if(resultTable[i]) {
            let r = resultTable[i]
            let td = document.createElement('td');
            td.appendChild(document.createTextNode(`Клики: ${r.click}`));
            tr.appendChild(td)
            td = document.createElement('td');
            td.appendChild(document.createTextNode(`Ошибки: ${r.error}`));
            tr.appendChild(td)
            td = document.createElement('td');
            td.appendChild(document.createTextNode(`Продолжительность: ${r.duration/1000} сек`));
            tr.appendChild(td)
            td = document.createElement('td');
            td.appendChild(document.createTextNode(`Когда: ${new Date(r.start).toLocaleString("ru")} `));
            tr.appendChild(td)

            tbdy.appendChild(tr);
        }
    }
    tbl.appendChild(tbdy);
    body.appendChild(tbl)
}

let Preloader = new Phaser.Class({

    Extends: Phaser.Scene,

    initialize:

        function Preloader() {
            Phaser.Scene.call(this, 'preloader');
        },

    preload: function () {
        for (let i = 0; i < 10; i++) {
            this.load.image(`${i}`, `assets/${i}.jpg`)
        }
        this.load.image('rubashka', 'assets/rubashka.png');
        this.load.image('play-button', 'assets/play-button.png');
    },

    create: function () {
        this.scene.start('menu');
    }

});

let Menu = new Phaser.Class({
    Extends: Phaser.Scene,

    initialize:

        function Game() {
            Phaser.Scene.call(this, 'menu');
        },

    create: function () {
        let start = this.add.image(game.config.width / 2,
            game.config.height / 2, 'play-button');
        start.setInteractive()

        start.once('pointerup', function () {
            console.log('start')
            let t1 = this.scene.transition({
                target: 'game',
                duration: 0,
                moveAbove: true
            });

        }, this);
    }

})

let Game = new Phaser.Class({

    Extends: Phaser.Scene,

    initialize:

        function Game() {
            Phaser.Scene.call(this, 'game');
        },

    create: function () {
        //x ширина
        //y высота

        let resultTable = JSON.parse(localStorage.getItem("resultTable")) || []
        console.log("resultTable", resultTable)
        let result = {click: 0, error: 0, start: new Date()}
        let start;
        let finish;
        let xOffset = 250 //Math.round(window.innerWidth * 0.1);
        let yOffset = 130 //Math.round(window.innerHeight * 0.1);
        let stepX = 120//Math.round((window.innerWidth - xOffset * 2) / 5);
        let stepY = 170//Math.round((window.innerHeight - yOffset * 2) / 4);
        let xScale = 0.5 //((stepX / sp.width) * 0.9).toFixed(2);
        let yScale = 0.5 //((stepY / sp.height) * 0.9).toFixed(2);


        let flip = false;
        let previousCard;
        let cardCounter = 0;

        let CounterCheck = () => {
            if (cardCounter === 0) {
                finish = new Date()
                result["duration"] = finish - start
                console.log(result)
                resultTable.unshift(result)
                console.log(resultTable)
                if (resultTable.length > 50) {
                    resultTable.length = 50
                }
                localStorage.setItem("resultTable", JSON.stringify(resultTable))
                tableCreate()
                this.scene.transition({
                    target: 'menu',
                    duration: 500,
                    moveAbove: true
                });
            }
        }

        let PictArray = [
            1, 2, 3, 4, 5,
            1, 2, 3, 4, 5,
            6, 7, 8, 9, 0,
            6, 7, 8, 9, 0
        ]

        let spritesArr = []


        console.log(stepX, stepY)
        switchControl(false)
        setTimeout(() => {
            spritesArr.forEach(s => {
                s.setTexture('rubashka')
            })
            switchControl(true)
            start = new Date()
        }, 5000)
        for (let y = 0; y < 4; y++) {
            for (let x = 0; x < 5; x++) {
                let xF = stepX * x + xOffset
                let yF = stepY * y + yOffset
                cardCounter++;
                let sprite = this.add.sprite(xF, yF, 'rubashka')
                    .setInteractive()
                    .setScale(xScale, yScale);
                spritesArr.push(sprite)
                let randElemIndex = Math.floor(Math.random() * PictArray.length)
                let randElem = PictArray.splice(randElemIndex, 1)
                // console.log(randElem)
                sprite.myVol = randElem[0];
                sprite.on('pointerdown', function (pointer) {
                    if (!controlEnable){
                        return;
                    }
                    if (this.myFlipped) {
                        return
                    }
                    result.click++
                    // console.log(this.x, this.y)
                    this.setTexture(`${this.myVol}`)//.setScale(cardScale)
                    this.setScale(xScale, yScale)
                    console.log(this.myVol)
                    this.myFlipped = true;
                    if (!flip) {
                        previousCard = this
                        flip = true;
                    } else {
                        switchControl(false)
                        flip = false;
                        if (previousCard.myVol === this.myVol) {
                            console.log('УИИИИ ты нашёл пару')
                            cardCounter--;
                            cardCounter--;
                            switchControl(true)
                            CounterCheck()
                        } else {
                            setTimeout(() => {
                                result.error++
                                switchControl(true)
                                previousCard.setTexture('rubashka')
                                previousCard.myFlipped = false;
                                this.myFlipped = false;
                                this.setTexture('rubashka')
                                this.setScale(xScale, yScale)
                                previousCard.setScale(xScale, yScale)
                                previousCard = undefined

                            }, 700)
                        }
                    }
                });
                sprite.on('pointerout', function (pointer) {
                    this.clearTint();
                });
                sprite.on('pointerup', function (pointer) {
                    this.clearTint();
                });
            }
        }
        spritesArr.forEach(s => {
            s.setTexture(`${s.myVol}`)
        })
    }
});

let switchControl = (status) => {
    controlEnable = status
    return
    try {
        game.input.touch.enabled = status;
    } catch (e) {
    }
    try {
        game.input.mouse.enabled = status;
    } catch (e) {
    }
    try {
        game.input.keyboard.enabled = status;
    } catch (e) {
    }
}

let config = {
    type: Phaser.AUTO,
    parent: "theGame",
    width: 1024,//window.innerWidth * 0.95,
    height: 768, //window.innerHeight * 0.95,
    physics: {
        default: 'arcade',
        arcade: {
            gravity: {y: 0},
            debug: false
        }
    },
    scene: [Preloader, Menu, Game]
};
tableCreate()
let game = new Phaser.Game(config);
